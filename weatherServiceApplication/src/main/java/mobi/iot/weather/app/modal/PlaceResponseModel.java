package mobi.iot.weather.app.modal;

import mobi.iot.weather.library.model.PlaceModel;

import java.util.Date;
import java.util.List;

public class PlaceResponseModel {
    private String name;

    private Date date;

    private String dayPhenomenon;

    private String nightPhenomenon;

    private String nightText;

    private String dayText;

    private int dayTempmax;

    private int nightTempmax;

    private int dayTempmin;

    private int nightTempmin;


    public PlaceResponseModel(){

    }

    public PlaceResponseModel(List<PlaceModel> placeModels){
        this.name = placeModels.get(0).getName();

        /* Since we have seperate day and night entities, we need to aggregate them here.
         * better to change to a hirearchical breakdown */
        for (PlaceModel placeModel : placeModels) {
            if (placeModel.getDay() != null) {
                this.dayPhenomenon = placeModel.getPhenomenon();
                this.dayTempmax = placeModel.getTempmax();
                this.dayText = placeModel.getDay().getText();
                this.dayTempmin = placeModel.getDay().getTempmin();
                this.date = placeModel.getDay().getForecast().getDate();
            } else {
                this.nightPhenomenon = placeModel.getPhenomenon();
                this.nightTempmax = placeModel.getTempmax();
                this.nightText = placeModel.getNight().getText();
                this.nightTempmin = placeModel.getNight().getTempmin();
            }
        }
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Date getDate ()
    {
        return date;
    }

    public void setDate (Date date)
    {
        this.date = date;
    }

    public String getDayPhenomenon ()
    {
        return dayPhenomenon;
    }

    public void setDayPhenomenon (String dayPhenomenon)
    {
        this.dayPhenomenon = dayPhenomenon;
    }

    public String getNightPhenomenon ()
    {
        return nightPhenomenon;
    }

    public void setNightPhenomenon (String nightPhenomenon)
    {
        this.nightPhenomenon = nightPhenomenon;
    }

    public int getDayTempmax ()
    {
        return dayTempmax;
    }

    public void setDayTempmax (int dayTempmax)
    {
        this.dayTempmax = dayTempmax;
    }

    public int getNightTempmax ()
    {
        return nightTempmax;
    }

    public void setNightTempmax (int nightTempmax)
    {
        this.nightTempmax = nightTempmax;
    }

    public String getDayText ()
    {
        return dayText;
    }

    public void setDayText (String dayText)
    {
        this.dayText = dayText;
    }

    public String getNightText ()
    {
        return nightText;
    }

    public void setNightText (String nightText)
    {
        this.nightText = nightText;
    }

    public int getDayTempmin ()
    {
        return dayTempmin;
    }

    public void setDayTempmin (int dayTempmin)
    {
        this.dayTempmin = dayTempmin;
    }

    public int getNightTempmin ()
    {
        return nightTempmin;
    }

    public void setNightTempmin (int nightTempmin)
    {
        this.nightTempmax = nightTempmin;
    }
}
