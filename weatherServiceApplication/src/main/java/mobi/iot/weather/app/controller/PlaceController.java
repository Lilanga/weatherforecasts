package mobi.iot.weather.app.controller;

import mobi.iot.weather.app.http.PlaceNotFoundException;
import mobi.iot.weather.app.modal.PlaceResponseModel;
import mobi.iot.weather.library.model.PlaceModel;
import mobi.iot.weather.library.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/places")
public class PlaceController {

    private final PlaceService placeService;

    @Autowired
    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<PlaceModel> dayList(){

        return this.placeService.findAllPlaces()
                .stream().filter(place->place.getDay() != null)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public PlaceResponseModel findByName(@PathVariable("name") String name){
        List<PlaceModel> model = this.placeService.findForecastsByPlaceName(name);

        if(model.size() == 0){
            throw new PlaceNotFoundException();
        }

        return new PlaceResponseModel(model);
    }
}
