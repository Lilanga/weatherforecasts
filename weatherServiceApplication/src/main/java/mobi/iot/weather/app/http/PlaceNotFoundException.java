package mobi.iot.weather.app.http;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "place is not found in the database")
public class PlaceNotFoundException extends RuntimeException{
}
