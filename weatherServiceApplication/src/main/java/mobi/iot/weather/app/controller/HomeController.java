package mobi.iot.weather.app.controller;
import mobi.iot.weather.library.service.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class HomeController {

    private final UpdateService updateService;

    @Autowired
    public HomeController(UpdateService updateService) {
        this.updateService = updateService;
    }

    // this route is used to force manual update, for testing
    @RequestMapping("/update")
    public String update() throws IOException {
        return updateService.updateLatestForecast() ? "Success" : "No updates";
    }
}
