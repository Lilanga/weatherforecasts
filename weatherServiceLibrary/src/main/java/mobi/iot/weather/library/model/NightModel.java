package mobi.iot.weather.library.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import mobi.iot.weather.library.model.xml.Day;
import mobi.iot.weather.library.model.xml.Night;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "night")
public class NightModel extends AuditModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId ()
    {
        return id;
    }

    public void setId (long id)
    {
        this.id = id;
    }

    @Column(columnDefinition = "text")
    private String phenomenon;

    private int tempmax;

    private int tempmin;

    @Column(columnDefinition = "text")
    private String text;

    @Column(columnDefinition = "text")
    private String peipsi;

    @JsonBackReference
    @OneToOne(cascade = {CascadeType.ALL})
    private ForecastModel forecast;

    @JsonManagedReference
    @OneToMany(targetEntity= PlaceModel.class, mappedBy="night", fetch=FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<PlaceModel> places;

    public NightModel() {}

    public NightModel(Night night) {
        this.phenomenon = night.getPhenomenon();
        this.tempmax = night.getTempmax();
        this.tempmin = night.getTempmin();
        this.peipsi = night.getPeipsi();
        this.text = night.getText();

        List<PlaceModel> places = new ArrayList<>();

        night.getPlaces().forEach((place) ->{
            PlaceModel model = new PlaceModel(place);
            model.setNight(this);
            places.add(model);
        });

        this.setPlaces(places);
    }

    public List<PlaceModel> getPlaces ()
    {
        return places;
    }

    public void setPlaces (List<PlaceModel> places)
    {
        this.places = places;
    }

    public String getPeipsi ()
    {
        return peipsi;
    }

    public void setPeipsi (String peipsi)
    {
        this.peipsi = peipsi;
    }

    public String getPhenomenon ()
    {
        return phenomenon;
    }

    public void setPhenomenon (String phenomenon)
    {
        this.phenomenon = phenomenon;
    }

    public int getTempmax ()
    {
        return tempmax;
    }

    public void setTempmax (int tempmax)
    {
        this.tempmax = tempmax;
    }

    public int getTempmin ()
    {
        return tempmin;
    }

    public void setTempmin (int tempmin)
    {
        this.tempmin = tempmin;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public ForecastModel setForecast ()
    {
        return forecast;
    }

    public void setForecast (ForecastModel forecast)
    {
        this.forecast = forecast;
    }

    @Override
    public String toString()
    {
        return "[phenomenon = "+phenomenon+", tempmax = "+tempmax+", tempmin = "+tempmin+", text = "+text+"]";
    }
}