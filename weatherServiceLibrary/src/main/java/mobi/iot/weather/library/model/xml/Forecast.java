package mobi.iot.weather.library.model.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.Date;

public class Forecast
{
    private Night night;

    private Day day;

    @JacksonXmlProperty(isAttribute = true)
    private Date date;

    public Night getNight ()
    {
        return night;
    }

    public void setNight (Night night)
    {
        this.night = night;
    }

    public Day getDay ()
    {
        return day;
    }

    public void setDay (Day day)
    {
        this.day = day;
    }

    public Date getDate ()
    {
        return date;
    }

    public void setDate (Date date)
    {
        this.date = date;
    }

    @Override
    public String toString()
    {
        return "[night = "+night+", day = "+day+", date = "+date+"]";
    }
}