package mobi.iot.weather.library.model.xml;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Forecasts
{
    @JacksonXmlElementWrapper(useWrapping = false, localName = "forecast")
    private List<Forecast> forecast;

    public List<Forecast>  getForecast ()
    {
        return forecast;
    }

    public void setForecast (List<Forecast> forecast)
    {
        this.forecast = forecast;
    }

    /* this utility method returns the latest forecast, which is also have places associated with it. */
    @JsonIgnore
    public Optional<Forecast> getLatestForecast() {
        return forecast.stream().filter(p->( p.getNight().getPlaces() != null || p.getDay().getPlaces() != null))
                .sorted(Comparator.nullsLast(
                        (f1, f2) -> f2.getDate().compareTo(f1.getDate())))
                .findFirst();
    }

    @Override
    public String toString()
    {
        return "[forecast = "+forecast+"]";
    }
}
