package mobi.iot.weather.library.repository;

import mobi.iot.weather.library.model.ForecastModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface ForecastRepository extends JpaRepository<ForecastModel, Long> {
    ForecastModel findByDate(Date date);
    boolean existsByDate(Date date);
}
