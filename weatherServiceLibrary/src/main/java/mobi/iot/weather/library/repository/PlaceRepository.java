package mobi.iot.weather.library.repository;

import mobi.iot.weather.library.model.PlaceModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlaceRepository extends JpaRepository<PlaceModel, Long> {
    List<PlaceModel> findByName(String name);
}
