package mobi.iot.weather.library.service;

import mobi.iot.weather.library.model.ForecastModel;
import mobi.iot.weather.library.model.xml.Forecast;
import mobi.iot.weather.library.repository.ForecastRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
public class UpdateService {

    private ForecastService forecastService;
    private ForecastRepository forecastRepository;

    @Autowired
    public UpdateService(ForecastService forecastService, ForecastRepository forecastRepository){
        this.forecastService = forecastService;
        this.forecastRepository = forecastRepository;
    }

    // Retrieve latest forecast from service url and update to the database if it is newer
    public boolean updateLatestForecast() throws IOException {

        ForecastModel model = null;
        Optional<Forecast> xmlForecast = this.forecastService.fetchForecasts().getLatestForecast();

        if(xmlForecast.isPresent()){
            /* Check whether we already have this forecast in the database
            * If not, we can update the record*/
            if(!this.forecastRepository.existsByDate(xmlForecast.get().getDate())){
                model = this.forecastRepository.saveAndFlush(new ForecastModel(xmlForecast.get()));
            }
        }

        return model != null;
    }
}
