package mobi.iot.weather.library.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import mobi.iot.weather.library.model.xml.Forecast;
import mobi.iot.weather.library.model.xml.Night;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "forecast")
public class ForecastModel extends AuditModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne(cascade = {CascadeType.ALL})
    @JsonManagedReference
    private NightModel night;

    @OneToOne(cascade = {CascadeType.ALL})
    @JsonManagedReference
    private DayModel day;

    private Date date;

    public ForecastModel() {}

    public ForecastModel(Forecast forecast) {
        this.date = forecast.getDate();

        NightModel nightModel = new NightModel(forecast.getNight());
        nightModel.setForecast(this);

        DayModel dayModel = new DayModel(forecast.getDay());
        dayModel.setForecast(this);

        this.setNight(nightModel);
        this.setDay(dayModel);
    }

    public long getId ()
    {
        return id;
    }

    public void setId (long id)
    {
        this.id = id;
    }

    public NightModel getNight ()
    {
        return night;
    }

    public void setNight (NightModel night)
    {
        this.night = night;
    }

    public DayModel getDay ()
    {
        return day;
    }

    public void setDay (DayModel day)
    {
        this.day = day;
    }

    public Date getDate ()
    {
        return date;
    }

    public void setDate (Date date)
    {
        this.date = date;
    }

    @Override
    public String toString()
    {
        return "[night = "+night+", day = "+day+", date = "+date+"]";
    }
}