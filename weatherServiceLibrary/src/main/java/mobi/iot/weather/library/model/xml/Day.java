package mobi.iot.weather.library.model.xml;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.ArrayList;
import java.util.List;

public class Day
{
    private String phenomenon;

    private int tempmax;

    private int tempmin;

    private String text;

    private String peipsi;

    @JacksonXmlElementWrapper(localName = "places")
    @JsonProperty("places")
    private List<Place> places;

    @JsonSetter
    public void setPlace(Place place) {

        if(this.places == null){
            this.places = new ArrayList<Place>();
        }

        this.places.add(place);
    }

    public List<Place> getPlaces ()
    {
        return places;
    }

    public void setPlaces (List<Place> places)
    {
        this.places = places;
    }

    public String getPeipsi ()
    {
        return peipsi;
    }

    public void setPeipsi (String peipsi)
    {
        this.peipsi = peipsi;
    }

    public String getPhenomenon ()
    {
        return phenomenon;
    }

    public void setPhenomenon (String phenomenon)
    {
        this.phenomenon = phenomenon;
    }

    public int getTempmax ()
    {
        return tempmax;
    }

    public void setTempmax (int tempmax)
    {
        this.tempmax = tempmax;
    }

    public int getTempmin ()
    {
        return tempmin;
    }

    public void setTempmin (int tempmin)
    {
        this.tempmin = tempmin;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    @Override
    public String toString()
    {
        return "[phenomenon = "+phenomenon+", tempmax = "+tempmax+", tempmin = "+tempmin+", text = "+text+"]";
    }
}
