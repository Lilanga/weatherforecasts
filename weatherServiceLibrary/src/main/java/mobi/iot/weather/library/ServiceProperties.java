package mobi.iot.weather.library;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("service")
public class ServiceProperties {

    private String weatherService;

    public String getWeatherService() {
        return weatherService;
    }

    public void setWeatherService(String weatherService) {
        this.weatherService = weatherService;
    }

    private String userAgent;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
