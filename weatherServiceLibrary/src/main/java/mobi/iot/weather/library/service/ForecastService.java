package mobi.iot.weather.library.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import mobi.iot.weather.library.ServiceProperties;
import mobi.iot.weather.library.model.xml.Forecasts;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

@Component
@Service
@EnableConfigurationProperties(ServiceProperties.class)
public class ForecastService {

    private final ServiceProperties serviceProperties;

    public ForecastService(ServiceProperties serviceProperties) {
        this.serviceProperties = serviceProperties;
    }

    public Forecasts fetchForecasts() throws IOException {
        String xml = getXmlStringFromService();
        return getForecastsFromXml(xml);
    }

    private Forecasts getForecastsFromXml(String xml) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return xmlMapper.readValue(xml, Forecasts.class);
    }

    private URLConnection openConnection() throws IOException {
        URL url = new URL(this.serviceProperties.getWeatherService());
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("User-Agent", this.serviceProperties.getUserAgent());
        connection.connect();
        return connection;
    }

    private String getXmlStringFromService() throws IOException {
        URLConnection connection = openConnection();
        return inputStreamToString(connection.getInputStream());
    }

    private static String inputStreamToString(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = br.readLine()) != null) {
            stringBuilder.append(line);
        }
        br.close();
        return stringBuilder.toString();
    }
}
