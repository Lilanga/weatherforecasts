package mobi.iot.weather.library.model.xml;

public class Place
{
    private String phenomenon;

    private int tempmax;

    private String name;

    public String getPhenomenon ()
    {
        return phenomenon;
    }

    public void setPhenomenon (String phenomenon)
    {
        this.phenomenon = phenomenon;
    }

    public int getTempmax ()
    {
        return tempmax;
    }

    public void setTempmax (int tempmax)
    {
        this.tempmax = tempmax;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "[phenomenon = "+phenomenon+", tempmax = "+tempmax+", name = "+name+"]";
    }
}
