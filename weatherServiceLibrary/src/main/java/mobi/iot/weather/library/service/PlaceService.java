package mobi.iot.weather.library.service;

import mobi.iot.weather.library.model.PlaceModel;
import mobi.iot.weather.library.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlaceService {
    private PlaceRepository placeRepository;

    @Autowired
    public PlaceService(PlaceRepository placeRepository){
        this.placeRepository = placeRepository;
    }

    public Optional<PlaceModel> findPlaceById(Long id) {
        return this.placeRepository.findById(id);
    }

    public List<PlaceModel> findAllPlaces() {
        return this.placeRepository.findAll();
    }

    @EntityGraph(value = "XmlPlace.days.forecast", type = EntityGraph.EntityGraphType.FETCH)
    public List<PlaceModel> findForecastsByPlaceName(String name) {
        return this.placeRepository.findByName(name);
    }

}
