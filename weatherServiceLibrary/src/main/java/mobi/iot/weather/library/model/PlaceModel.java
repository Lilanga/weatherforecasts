package mobi.iot.weather.library.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import mobi.iot.weather.library.model.xml.Place;

import javax.persistence.*;

@Entity
@Table(name = "place")
public class PlaceModel extends AuditModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId ()
    {
        return id;
    }

    public void setId (long id)
    {
        this.id = id;
    }

    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.ALL})
    private DayModel day;

    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.ALL})
    private NightModel night;

    @Column(columnDefinition = "text")
    private String phenomenon;

    private int tempmax;

    private String name;

    public PlaceModel() {}

    public PlaceModel(Place place) {
        this.phenomenon = place.getPhenomenon();
        this.tempmax = place.getTempmax();
        this.name = place.getName();
    }

    public DayModel getDay ()
    {
        return day;
    }

    public void setDay (DayModel day)
    {
        this.day = day;
    }

    public NightModel getNight ()
    {
        return night;
    }

    public void setNight (NightModel night)
    {
        this.night = night;
    }

    public String getPhenomenon ()
    {
        return phenomenon;
    }

    public void setPhenomenon (String phenomenon)
    {
        this.phenomenon = phenomenon;
    }

    public int getTempmax ()
    {
        return tempmax;
    }

    public void setTempmax (int tempmax)
    {
        this.tempmax = tempmax;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "[phenomenon = "+phenomenon+", tempmax = "+tempmax+", name = "+name+"]";
    }
}
