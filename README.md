# Weather Service Application

## Installation

> - We need to have PostgreSQL running locally (localhost:5432)
> - Database should be configured with nessasary permissions for the `weather` user
> - Application is targeted for Java 1.8, JDK 1.8, Maven build system


## Commands
> - **Using console, goto project's root folder** : cd ~/source/
> - **Use Maven Wrapper to install dependencies** : $ ./mvnw install
> - **Use Maven Wrapper to run the application** : $ ./mvnw spring-boot:run -pl weatherServiceApplication

## When App is running

> - App is starting with default 8080 at localhost:8080
> - If you want to syncronize manually without waiting for the 30 minutes CRON, call `http://localhost:8080/update` route


## Documentation
#### Frontend application
* **Frontend**:
Frontend application is developed using reactjs and display list of cities initially.
Please use place search box to search for the latest weather forecast of given place from the database. 