package mobi.iot.weather.scheduler;

import mobi.iot.weather.library.service.UpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduledTasks {

    private UpdateService updateService;

    @Autowired
    public ScheduledTasks(UpdateService updateService){
        this.updateService = updateService;
    }

    // Schedule forecast service call in 30 minutes to fetch and update database
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    // CRON is running for every 30 minutes
    @Scheduled(cron = "0 0/30 * * * ?")
    public void updateWetherForecasts() {
        try {
            if(this.updateService.updateLatestForecast()){
                log.info("Forecast is updated successfully at {}", dateFormat.format(new Date()));
            }
        }catch (IOException exp){
            log.error(exp.getLocalizedMessage(), exp);
        }
    }
}